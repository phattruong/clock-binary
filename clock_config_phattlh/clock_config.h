/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __STM8S_CLK_CF_H
#define __STM8S_CLK_CF_H
#include "stm8s.h"

void Clock_Init_16MHZ_HSI(void);
void Clock_Init_128K_LSI(void);

#endif
