#include <clock_config.h>


void Clock_Init_16MHZ_HSI(){
  /* reset l?i t?t c? c�c thanh ghi c?a Clock 
  config 16Mhz
  
  config clock
  khoi tao gia tri mac dinh
  chon bo chia cua cpu
  chon bo chia cua hsi 
  confirm sw clock
  
  */
  CLK_DeInit();
  CLK_SYSCLKConfig(CLK_PRESCALER_CPUDIV1);
  CLK_SYSCLKConfig(CLK_PRESCALER_HSIDIV1);
  CLK_ClockSwitchConfig(CLK_SWITCHMODE_AUTO,CLK_SOURCE_HSI, DISABLE, CLK_CURRENTCLOCKSTATE_DISABLE);
  
  /* agru 3 interupt bo ngat khi config log
  */
  
}
void Clock_Init_128K_LSI(){
 /* reset l?i t?t c? c�c thanh ghi c?a Clock */
  CLK_DeInit();
  /* c?u h�nh Fcpu ? ch? d? chia 1, th� Fcpu v?n l� 128kHz */
  CLK_SYSCLKConfig(CLK_PRESCALER_CPUDIV1);
  /* chuy?n ngu?n clock qua d�ng LSI*/
  CLK_ClockSwitchConfig(CLK_SWITCHMODE_AUTO,CLK_SOURCE_LSI, DISABLE, CLK_CURRENTCLOCKSTATE_DISABLE);
}