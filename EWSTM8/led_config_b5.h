/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __STM8S_LED_CF_H
#define __STM8S_LED_CF_H

#include "stm8s.h"

#define LED_PORT  GPIOB
#define LED_PIN   GPIO_PIN_5

void delay(uint32_t i);
void Led_Init_GPIOB_PIN_5(void);
void Led_Blink_GPIOB_PIN_5(uint32_t i);
void Led_On_GPIOB_PIN_5(void);
void Led_Off_GPIOB_PIN_5(void);

#endif