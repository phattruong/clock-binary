#include "stm8s.h"
#include "led_config_b5.h"

void delay(uint32_t i)
{
  while(i-- >0)
}

void Led_Init_GPIOB_PIN_5()
{
  GPIO_Init( GPIOB, GPIO_Pin_5, GPIO_MODE_OUT_PP_LOW_FAST);
}

void Led_Blink_GPIOB_PIN_5(uint32_t delay)
{
  GPIO_WriteReverse( GPIOB, GPIO_Pin_5);
  while(delay-- >0)
}

void Led_On_GPIOB_PIN_5()
{
  GPIO_WriteLow( GPIOB, GPIO_Pin_5);
}

void Led_Off_GPIOB_PIN_5()
{
  GPIO_WriteHigh( GPIOB, GPIO_Pin_5);
}