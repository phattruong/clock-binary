#include "i2c_drv.h"

//������� �������� ������� I2C
static unsigned long int i2c_timeout;

//������ ������� � �������������
#define set_tmo_us(time)  i2c_timeout = (unsigned long int)(F_MASTER_MHZ * time)

//������ ������� � �������������
#define set_tmo_ms(time)  i2c_timeout = (unsigned long int)(F_MASTER_MHZ * time * 1000)

#define tmo               i2c_timeout--

//�������� ����������� ������� event
//� ������� ������� timeout � ��
#define wait_event(event, timeout) set_tmo_ms(timeout);\
                                   while(event && --i2c_timeout);\
                                   if(!i2c_timeout) return I2C_TIMEOUT;
                                   
                                   
//******************************************************************************
// ������������� I2C ����������      
//      f_master_hz - ������� ������������ ��������� Fmaster                                   
//      f_i2c_hz - �������� �������� ������ �� I2C                                   
//******************************************************************************                                   
void i2c_master_init(unsigned long f_master_hz, unsigned long f_i2c_hz)
{
    unsigned long int ccr;

    //PB_DDR_bit.DDR4 = 0;
    //PB_DDR_bit.DDR5 = 0;
    //PB_ODR_bit.ODR5 = 1;  //SDA
    //PB_ODR_bit.ODR4 = 1;  //SCL
    GPIOB->DDR &= ~0x30;
    GPIOB->ODR |= 0x30;
    
    //PB_CR1_bit.C14 = 0;
    //PB_CR1_bit.C15 = 0;
    GPIOB->CR1 &= ~0x30;

    //PB_CR2_bit.C24 = 0;
    //PB_CR2_bit.C25 = 0;
    GPIOB->CR2 &= ~0x30;
    
    //������� ������������ ��������� MHz
    //I2C_FREQR_FREQ = 12;
    I2C->FREQR = 12;
    //��������� I2C
    //I2C_CR1_PE = 0;
    I2C->CR1 &= ~I2C_CR1_PE;
    //� ����������� ������ �������� I2C max = 100 ����/�
    //�������� ����������� ����� 
    //I2C_CCRH_F_S = 0;
    I2C->CCRH &= ~0x80;
    //CCR = Fmaster/2*Fiic= 12MHz/2*100kHz
    ccr = f_master_hz/(2*f_i2c_hz);
    //Set Maximum Rise Time: 1000ns max in Standard Mode
    //= [1000ns/(1/InputClockFrequencyMHz.10e6)]+1
    //= InputClockFrequencyMHz+1
    //I2C_TRISER_TRISE = 12+1;
    I2C->TRISER = 13;
    //I2C_CCRL = ccr & 0xFF;
    I2C->CCRL = ccr & 0xFF;
    //I2C_CCRH_CCR = (ccr >> 8) & 0x0F;
    I2C->CCRH = (ccr >> 8) & 0x0F;
    //�������� I2C
    //I2C_CR1_PE = 1;
    I2C->CR1 |= I2C_CR1_PE;
    //��������� ������������� � ����� �������
    //I2C_CR2_ACK = 1;
    I2C->CR2 |= I2C_CR2_ACK;
}

//******************************************************************************
// ������ �������� slave-����������
//******************************************************************************                                   
t_i2c_status i2c_wr_reg(unsigned char address, unsigned char reg_addr,
                              char * data, unsigned char length)
{                                  
                
    //���� ������������ ���� I2C
    //wait_event(I2C_SR3_BUSY, 10);
    wait_event((I2C->SR3 & 2), 10);

    //��������� �����-�������
    //I2C_CR2_START = 1;
    I2C->CR2 |= I2C_CR2_START;
    //���� ��������� ���� SB
    //wait_event(!I2C_SR1_SB, 1);
    wait_event(((I2C->SR1 & 1)==0), 1);


    //���������� � ������� ������ ����� �������� ����������
    //I2C_DR = address & 0xFE;
    I2C->DR = address & 0xFE;
    //���� ������������� �������� ������
    //wait_event(!I2C_SR1_ADDR, 1);
    wait_event(!(I2C->SR1 & 2), 1);
    //������� ���� ADDR ������� �������� SR3
    //I2C_SR3;
    I2C->SR3;


    //���� ������������ �������� ������
    //wait_event(!I2C_SR1_TXE, 1);
    wait_event(!(I2C->SR1 & 0x80), 1);
    //���������� ����� ��������
    //I2C_DR = reg_addr;
    I2C->DR = reg_addr;

    //�������� ������
    while(length--)
    {
        //���� ������������ �������� ������
        //wait_event(!I2C_SR1_TXE, 1);
        wait_event(!(I2C->SR1 & 0x80), 1);
        //���������� ����� ��������
        //I2C_DR = *data++;
        I2C->DR = *data++;
    }

    //����� ������, ����� DR ����������� � ������ ������ � ��������� �������
    //wait_event(!(I2C_SR1_TXE && I2C_SR1_BTF), 1);
    wait_event(((I2C->SR1 & 0x84) != 0x84), 1);

    //�������� ����-�������
    //I2C_CR2_STOP = 1;
    I2C->CR2 |= I2C_CR2_STOP;
    //���� ���������� ������� ����
    //wait_event(I2C_CR2_STOP, 1);
    wait_event((I2C->CR2 & 2), 1);

    return I2C_SUCCESS;
}

//******************************************************************************
// ������ �������� slave-����������
// Start -> Slave Addr -> Reg. addr -> Restart -> Slave Addr <- data ... -> Stop 
//******************************************************************************                                   
t_i2c_status i2c_rd_reg(unsigned char address, unsigned char reg_addr,
                              char * data, unsigned char length)
{

    //���� ������������ ���� I2C
   // wait_event(I2C_SR3_BUSY, 10);
    wait_event((I2C->SR3 & 2), 10);
    
    //��������� ������������� � ����� �������
    //I2C_CR2_ACK = 1;
    I2C->CR2 |= I2C_CR2_ACK;

    //��������� �����-�������
    //I2C_CR2_START = 1;
    I2C->CR2 |= I2C_CR2_START;
    //���� ��������� ���� SB
    //wait_event(!I2C_SR1_SB, 1);
    wait_event(((I2C->SR1 & 1)==0), 1);
    
    //���������� � ������� ������ ����� �������� ����������
    //I2C_DR = address & 0xFE;
    I2C->DR = address & 0xFE;
    //���� ������������� �������� ������
    //wait_event(!I2C_SR1_ADDR, 1);
    wait_event(!(I2C->SR1 & 2), 1);
    
    //������� ���� ADDR ������� �������� SR3
    //I2C_SR3;
    I2C->SR3;

    //���� ������������ �������� ������ RD
    //wait_event(!I2C_SR1_TXE, 1);
    wait_event(!(I2C->SR1 & 0x80), 1);
    
    //�������� ����� �������� slave-����������, ������� ����� ���������
    //I2C_DR = reg_addr;
    I2C->DR = reg_addr;
    //����� ������, ����� DR ����������� � ������ ������ � ��������� �������
    //wait_event(!(I2C_SR1_TXE && I2C_SR1_BTF), 1);
    wait_event(((I2C->SR1 & 0x84) != 0x84), 1);
    
    //��������� �����-������� (�������)
    //I2C_CR2_START = 1;
    I2C->CR2 |= I2C_CR2_START;
    //���� ��������� ���� SB
    //wait_event(!I2C_SR1_SB, 1);
    wait_event(((I2C->SR1 & 1)==0), 1);
    
    //���������� � ������� ������ ����� �������� ���������� � ���������
    //� ����� ������ (���������� �������� ���� � 1)
    //I2C_DR = address | 0x01;
    I2C->DR = address | 0x01;

    //������ �������� ������� �� ���������� ����������� ����
    //N=1
    if(length == 1)
    {
        //��������� ������������� � ����� �������
        //I2C_CR2_ACK = 0;
        I2C->CR2 &= ~I2C_CR2_ACK;
        //���� ������������� �������� ������
        //wait_event(!I2C_SR1_ADDR, 1);
        wait_event(!(I2C->SR1 & 2), 1);
        
        //�������� �� Errata
        __disable_interrupt();
        //������� ���� ADDR ������� �������� SR3
        //I2C_SR3;
        I2C->SR3;
        //����������� ��� STOP
        //I2C_CR2_STOP = 1;
        I2C->CR2 |= I2C_CR2_STOP;
        //�������� �� Errata
        __enable_interrupt();

        //���� ������� ������ � RD
        //wait_event(!I2C_SR1_RXNE, 1);
        wait_event(!(I2C->SR1 & I2C_SR1_RXNE), 1);
        
        //������ �������� ����
        //*data = I2C_DR;
        *data = I2C->DR;
    } 
    //N=2
    else if(length == 2)
    {
        //��� ������� ��������� NACK �� ��������� �������� �����
        //I2C_CR2_POS = 1;
        I2C->CR2 |= I2C_CR2_POS;
        //���� ������������� �������� ������
        wait_event(!I2C_SR1_ADDR, 1);
        //�������� �� Errata
        __disable_interrupt();
        //������� ���� ADDR ������� �������� SR3
        //I2C_SR3;
        I2C->SR3;
        //��������� ������������� � ����� �������
        //I2C_CR2_ACK = 0;
        I2C->CR2 &= ~I2C_CR2_ACK;
        //�������� �� Errata
        __enable_interrupt();
        //���� �������, ����� ������ ���� �������� � DR,
        //� ������ � ��������� ��������
        wait_event(!I2C_SR1_BTF, 1);

        //�������� �� Errata
        __disable_interrupt();
        //����������� ��� STOP
        //I2C_CR2_STOP = 1;
        I2C->CR2 |= I2C_CR2_STOP;
        //������ �������� �����
        //*data++ = I2C_DR;
        *data++ = I2C->DR;
        //�������� �� Errata
        __enable_interrupt();
        //*data = I2C_DR;
        *data = I2C->DR;
    } 
    //N>2
    else if(length > 2)
    {
        //���� ������������� �������� ������
        //wait_event(!I2C_SR1_ADDR, 1);
        wait_event(!(I2C->SR1 & 2), 1);
        
        //�������� �� Errata
        __disable_interrupt();

        //������� ���� ADDR ������� �������� SR3
        //I2C_SR3;
        I2C->SR3;

        //�������� �� Errata
        __enable_interrupt();

        while(length-- > 3 && tmo)
        {
            //������� ��������� ������ � DR � ��������� ��������
            //wait_event(!I2C_SR1_BTF, 1);
            wait_event(!(I2C->SR1 & I2C_SR1_BTF), 1);
            //������ �������� ���� �� DR
            //*data++ = I2C_DR;
            *data++ = I2C->DR;
        }
        //����� �������� �����
        if(!tmo) return I2C_TIMEOUT;

        //�������� ������� 3 ��������� �����
        //����, ����� � DR �������� N-2 ����, � � ��������� ��������
        //�������� N-1 ����
        //wait_event(!I2C_SR1_BTF, 1);
        wait_event(!(I2C->SR1 & I2C_SR1_BTF), 1);
        //��������� ������������� � ����� �������
        //I2C_CR2_ACK = 0;
        I2C->CR2 &= ~I2C_CR2_ACK;
        //�������� �� Errata
        __disable_interrupt();
        //������ N-2 ���� �� RD, ��� ����� �������� ������� � ���������
        //������� ���� N, �� ������ � ����� ������ ���������� ������� NACK
        //*data++ = I2C_DR;
        *data++ = I2C->DR;
        //������� STOP
        //I2C_CR2_STOP = 1;
        I2C->CR2 |= I2C_CR2_STOP;
        //������ N-1 ����
        //*data++ = I2C_DR;
        *data++ = I2C->DR;
        //�������� �� Errata
        __enable_interrupt();
        //����, ����� N-� ���� ������� � DR �� ���������� ��������
        //wait_event(!I2C_SR1_RXNE, 1);
        wait_event(!(I2C->SR1 & I2C_SR1_RXNE), 1);
        //������ N ����
        //*data++ = I2C_DR;
        *data++ = I2C->DR;
    }

    //���� �������� ���� �������
    //wait_event(I2C_CR2_STOP, 1);
    wait_event((I2C->CR2 & 2), 1);
    //���������� ��� POS, ���� ����� �� ��� ����������
    //I2C_CR2_POS = 0;
    I2C->CR2 &= ~I2C_CR2_POS;

    return I2C_SUCCESS;
}
