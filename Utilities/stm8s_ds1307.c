//******************************************************************************
// ������� RTC DS1307 ��� ���������������� STM8S003F
//
// �����: ������ ������
// ����:  18 ���� 2014
// URL:   http://hamlab.net/
//******************************************************************************

#include "stm8s_ds1307.h"

//******************************************************************************
//������� ����� �� ����������� ������������� � BCD-���
//******************************************************************************
static unsigned int ds1307_dec2bcd(unsigned int dec){

  unsigned int temp = dec, result;
  
  result = (unsigned int)(temp / 1000) << 12;
  temp %= 1000;
  result |= (temp/100) << 8;
  temp %= 100;
  result |= (temp/10) << 4;
  result |= temp % 10;
  
  return result;
}

//******************************************************************************
//������� ����� �� BCD-���� � ���������� �������������
//******************************************************************************
static unsigned int ds1307_bcd2dec(unsigned int bcd){

  unsigned int temp = bcd, result;
  
  result = (temp >> 12) * 1000;
  temp &= 0x0FFF;
  result += (temp >> 8) * 100;
  temp &= 0x00FF;
  result += (temp >> 4) * 10;
  temp &= 0x000F;
  result += temp;
  
  return result;
}

//******************************************************************************
//������� ������ ��������� ���������� �������� ds1307 �� ������ ���������
//���������� �������� � I2C
//******************************************************************************
static t_ds1307_status ds1307_i2c_error(t_i2c_status status){
    if(status != I2C_SUCCESS){
      switch(status){
        case I2C_TIMEOUT:  return DS1307_TIMEOUT; break;
        case I2C_ERROR:    return DS1307_ERROR;   break;
        default:           return DS1307_ERROR;   break;
      }
    } else {
        return DS1307_SUCCESS;
    }
}

//******************************************************************************
//�������� �� ������� ������ ���������� �������� I2C
//******************************************************************************
#define ds1307_check_error(i2c_status)\
  if(i2c_status != I2C_SUCCESS)\
    return ds1307_i2c_error(i2c_status)


//******************************************************************************
//������������� ����� DS1307
//******************************************************************************      
t_ds1307_status ds1307_init(t_ds1307_time_mode time_mode){
    
    //��������� ���������� �������� I2C
    t_i2c_status status = I2C_SUCCESS;
    
    //���������� ��� �������� ����������� ������
    t_ds1307_date_time data;
  
    //������������� RTC. ������ 0-� �������
    status = i2c_rd_reg(DS1307_SLAVE_ADDR, DS1307_REG_SEC, (char *)&data, 1);
    
    //�������� ��������� ���������� �������� �� I2C
    ds1307_check_error(status);
    
    //���� ������ ����� ���������, �� ��������� ������� CH=0
    if(data.ch){
      //����� ���� � �������
      return ds1307_reset(time_mode);
    }
  
    return DS1307_SUCCESS;
}

//******************************************************************************
//��������� ������� � ���� DS1307
//******************************************************************************
t_ds1307_status ds1307_set(t_ds1307_date_time * dt){
  
  //��������� ���������� �������� I2C
  t_i2c_status status = I2C_SUCCESS;
  
  //�� ��������� ������ �����
  dt->ch = 0;
  
  //��������� ���� � ����� � BCD-������
  dt->seconds = ds1307_dec2bcd(dt->seconds);
  dt->minutes = ds1307_dec2bcd(dt->minutes);
  dt->hours = ds1307_dec2bcd(dt->hours);
  dt->day = ds1307_dec2bcd(dt->day);
  dt->date = ds1307_dec2bcd(dt->date);
  dt->month = ds1307_dec2bcd(dt->month);
  dt->year = ds1307_dec2bcd(dt->year);
  
  //������ ��������� ds1307
  status = i2c_wr_reg(DS1307_SLAVE_ADDR, 
                      DS1307_REG_SEC, (char *)dt, 
                      sizeof(t_ds1307_date_time));
  
  //�������� ��������� ���������� �������� �� I2C
  ds1307_check_error(status);
  
  return DS1307_SUCCESS;
  
}

//******************************************************************************
//����� ������� � ���� DS1307
//******************************************************************************
t_ds1307_status ds1307_reset(t_ds1307_time_mode time_mode){
  
    //��������� ���������� �������� I2C
    t_i2c_status status = I2C_SUCCESS;
  
    //��������� �������� ������� � ����
    t_ds1307_date_time dt;
  
    //�������� ������������ �����
    dt.ch = 0;
    
    //����� �������
    dt.seconds = 0;
    dt.minutes = 0;
    dt.hours = 0;
    dt.mode = time_mode;
    
    //����� ����
    dt.date = 1;
    dt.month = 1;
    dt.year = 00;
    
    //������ ��������� ds1307
    status = i2c_wr_reg(DS1307_SLAVE_ADDR, 
                        DS1307_REG_SEC, (char *)&dt, 
                        sizeof(t_ds1307_date_time));
    
    //�������� ��������� ���������� �������� �� I2C
    ds1307_check_error(status);
    
    return DS1307_SUCCESS;
}

//******************************************************************************
//������ ������� � ���� �� ��������� DS1307
//******************************************************************************
t_ds1307_status ds1307_get(t_ds1307_date_time * dt){
  
  //��������� ���������� �������� I2C
  t_i2c_status status = I2C_SUCCESS;
  
  t_ds1307_date_time datetime;
  
  //������ ��������� ds1307
  status = i2c_rd_reg(DS1307_SLAVE_ADDR, 
                      DS1307_REG_SEC, (char *)&datetime, 
                      sizeof(t_ds1307_date_time));
  
  //�������� ��������� ���������� �������� �� I2C
  ds1307_check_error(status);
  
  //��������� ���� � ����� �� BCD-������� � �������
  dt->seconds = ds1307_bcd2dec(datetime.seconds);
  dt->minutes = ds1307_bcd2dec(datetime.minutes);
  dt->hours = ds1307_bcd2dec(datetime.hours);
  dt->day = ds1307_bcd2dec(datetime.day);
  dt->date = ds1307_bcd2dec(datetime.date);
  dt->month = ds1307_bcd2dec(datetime.month);
  dt->year = ds1307_bcd2dec(datetime.year);
  
  
  return DS1307_SUCCESS;
  
}

