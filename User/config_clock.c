#include "stm8s.h"

uint32_t x,x2;

/*
 config clock
    khoi tao gia tri mac dinh
    chon bo chia cua cpu
    chon bo chia cua hsi 
    confirm sw clock
*/

void main(void)
{
 
  CLK_DeInit(); // khoi tao thanh ghi
  
  CLK_SYSCLKConfig(CLK_PRESCALER_CPUDIV1); // config bo chia la 1

  CLK_ClockSwitchConfig(CLK_SWITCHMODE_AUTO,CLK_SOURCE_LSI, DISABLE, CLK_CURRENTCLOCKSTATE_DISABLE);
  

  
  while(1){

    x=CLK_GetClockFreq();
    x2=CLK_GetSYSCLKSource();
  }
}


#ifdef USE_FULL_ASSERT

void assert_failed(u8* file, u32 line)
{ 
/* User can add his own implementation to report the file name and line number,
ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

/* Infinite loop */
while (1)
{
}
}
#endif

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
