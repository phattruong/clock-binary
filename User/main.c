/**
  ******************************************************************************
  * @file    GPIO_Toggle\main.c
  * @author  MCD Application Team
  * @version  V2.2.0
  * @date     30-September-2014
  * @brief   This file contains the main function for GPIO Toggle example.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2014 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include "stm8s.h"
#include "i2c_drv.h"
#include "stm8s_ds1307.h"
#include "stdio.h"
//tao bien cho du lieu ds1307
t_ds1307_date_time ds1307_time;
#define F_I2C_HZ        100000UL
#define UART_BAUD       9600UL
#define F_MASTER_HZ     2000000UL
#define PUTCHAR_PROTOTYPE int putchar (int c)
#define GETCHAR_PROTOTYPE int getchar (void)
//#define CS_H  GPIO_WriteHigh(GPIOB, GPIO_PIN_1)
//#define CS_L  GPIO_WriteLow(GPIOB, GPIO_PIN_1)
//#define CLK_H  GPIO_WriteHigh(GPIOB, GPIO_PIN_0)
//#define CLK_L  GPIO_WriteLow(GPIOB, GPIO_PIN_0)
//#define DIN_H  GPIO_WriteHigh(GPIOB, GPIO_PIN_2)
//#define DIN_L  GPIO_WriteLow(GPIOB, GPIO_PIN_2)
#define CS_H  GPIO_WriteHigh(GPIOC, GPIO_PIN_4)
#define CS_L  GPIO_WriteLow(GPIOC, GPIO_PIN_4)
/**
  * @addtogroup GPIO_Toggle
  * @{
  */
__IO uint32_t TimingDelay = 0;
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Evalboard I/Os configuration */

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
void Delay_Basic (uint32_t nCount);
void TIM4_Config(void);
void CLK_Config(void);
void Delay_ms(__IO uint32_t nTime);
void TimingDelay_Decrement(void);
void Display_hour(char chhour);
void Display_minutes(char minutes);
void Display_second(char second);

////////////////////
/////////////////////////
void Shift(uint8_t addr, uint8_t data);
void Shutdown(char status);
void Intensity(int8_t value);
void ScanLimit();
void Display_Test();
void Display(char row, char column);
void Clear();
void MAX7219_SPI_Init();
//////////////////////////////
//char NPLLAB[49]={0,0xFE,0xFC,0x18,0x30,0x60,0xFE,0xFE,0,0xFE,0xFE,0x22,0x22,0x1C,0,0xFE,0xFE,0xC0,0xC0,0xC0,0,0,0xFE,0xFE,0xC0,0xC0,0xC0,0,0xFC,0xFE,0x36,0x36,0xFE,0xFC,0,0xFE,0xFE,0x88,0x88,0x70,0 ,0,0,0,0,0,0,0,0};
char NumBinary[10]={0,          //0
                    0x80,       //1
                    0x40,       //2
                    0xC0,       //3
                    0x20,       //4
                    0xA0,       //5
                    0x60,       //6
                    0xE0,       //7
                    0x10,       //8
                    0x90};      //9
////////////////////////////
void main(void)
{
  //bien trang thai DS1307
  t_ds1307_status ds1307_status;
  CLK_Config();
  //khoi tao UART1
//  UART1_DeInit();
//  UART1_Init((uint32_t)9600, UART1_WORDLENGTH_8D, UART1_STOPBITS_1, UART1_PARITY_NO,
//             UART1_SYNCMODE_CLOCK_DISABLE, UART1_MODE_TXRX_ENABLE);
//  printf("thoi gian thuc bat dau hoat dong\r\n");
  //
  MAX7219_SPI_Init();
  //
  Delay_Basic(500000);
  ScanLimit();
  Shutdown(0);
  Intensity(8);
  Clear();
  char data=0x01;
  Display(4,0xff);
  Display(5,0xff);
  i2c_master_init(F_MASTER_HZ, F_I2C_HZ);
  ds1307_status = ds1307_init(DS1307_TIME_MODE_24H);
  if(ds1307_status != DS1307_SUCCESS){
    //bao loi khi khong giao tiep duoc ds1307
    Display(8,0x44);
    while(1);
  }
  //bao ket noi thanh cong
    printf("DS1307 init oke\r\n");
    
    // cai dat ngay th�ng nam
    ds1307_time.date = 17;
    ds1307_time.month = 06;
    ds1307_time.year = 17;

    //cai dat gio phut giay
    ds1307_time.hours = 9;
    ds1307_time.minutes = 55;
    ds1307_time.seconds = 00;

    //set du lieu da dat cho ds1303
    ds1307_set(&ds1307_time);

  while (1)
  {
    /////////////
    /*
     test matrix
    Display(1,NumBinary[1]);
    Display(2,NumBinary[0]);
    Display(3,NumBinary[3]);
    Display(3,NumBinary[0]);
    */
    
    ds1307_get(&ds1307_time);
    Display_hour(ds1307_time.hours);
    Display_minutes(ds1307_time.minutes);
    Display_minutes(ds1307_time.seconds);
    
    printf("Time %02d :",ds1307_time.hours);
    printf(" %02d :",ds1307_time.minutes);
    printf(" %02d\r\n",ds1307_time.seconds);
    
    printf("Date %02d :",ds1307_time.date);
    printf(" %02d :",ds1307_time.month);
    printf(" 20%02d\r\n",ds1307_time.year);
    
    printf("-------------\r\n");
  }
}
//////////////////////////########################
void Display_hour(char chhour)
{
  int iHour= chhour - 48;
  int donvi = iHour % 10;
  int chuc = iHour / 10;
  Display(1,NumBinary[chuc]);
  Display(2,NumBinary[donvi]);
}
void Display_minutes(char minutes)
{
  int iMinutes= minutes - 48;
  int donvi = iMinutes % 10;
  int chuc = iMinutes / 10;
  Display(3,NumBinary[chuc]);
  Display(4,NumBinary[donvi]);
}
void Display_second(char second)
{
  int iSecond= second - 48;
  int donvi = iSecond % 10;
  int chuc = iSecond / 10;
  Display(5,NumBinary[chuc]);
  Display(6,NumBinary[donvi]);
}

char count_l=0, count_h=0;
void Shift(uint8_t addr, uint8_t data)
{
  CS_L;
  //
  SPI->DR = addr;
  while ((SPI->SR & SPI_SR_TXE) == 0)
  {
    /* Wait while the byte is transmitted */
  }
  ///
  SPI->DR = data;
  while ((SPI->SR & SPI_SR_TXE) == 0)
  {
    /* Wait while the byte is transmitted */
  }
  //
  Delay_Basic(5);
  ///
  CS_H;
}
/////////////////////////////#############################3
void Shutdown(char status)
{
  if(status==1) Shift(0x0c,0x00);
  else if(status==0) Shift(0x0c,0x01);
}
///////////////////////////#######################
void Intensity(int8_t value)
{
  if((value>=0)&(value<=15))
  Shift(0x0a,value);
}
///////////////////////////#######################
void Display_Test()
{
  Shift(0x0f,0x00);
}
//////////////////////////////#######################
void Display(char row, char column)//column 1->8, row 1->8
{
  Shift(row,column);
}
///////////////////////////#######################
void ScanLimit()
{
  Shift(0x0b,0x07);
}
//////////////////////////
void Clear()
{
  for(uint8_t i=1;i<9;i++)
  {
    Shift(i,0);
  }
}
//////////////////////////////########################3
void MAX7219_SPI_Init(void)
{
  /* Enable SPI clock */
  CLK_PeripheralClockConfig(CLK_PERIPHERAL_SPI, ENABLE);

  /* Set the MOSI and SCK at high level */
  //GPIO_ExternalPullUpConfig(GPIOC, (GPIO_Pin_TypeDef)(GPIO_PIN_6 |GPIO_PIN_5), ENABLE);

  /* SD_SPI Configuration */
  SPI_Init( SPI_FIRSTBIT_MSB, SPI_BAUDRATEPRESCALER_16, SPI_MODE_MASTER,
           SPI_CLOCKPOLARITY_LOW, SPI_CLOCKPHASE_1EDGE, SPI_DATADIRECTION_1LINE_TX,
           SPI_NSS_SOFT, 0x07);


  /* SD_SPI enable */
  SPI_Cmd( ENABLE);

  /* Set MSD ChipSelect pin in Output push-pull high level */
  GPIO_Init(GPIOC, GPIO_PIN_4, GPIO_MODE_OUT_PP_HIGH_SLOW);
  GPIO_WriteHigh(GPIOC, GPIO_PIN_4);//CS
}
//////////////////////////#######################
void CLK_Config(void)
{
    /* Initialization of the clock */
    /* Clock divider to HSI/1 */
    CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1);
}
////////////////////////###########################################
void TIM4_Config(void)
{
  /* TIM4 configuration:
   - TIM4CLK is set to 16 MHz, the TIM4 Prescaler is equal to 128 so the TIM1 counter
   clock used is 16 MHz / 128 = 125 000 Hz
  - With 125 000 Hz we can generate time base:
      max time base is 2.048 ms if TIM4_PERIOD = 255 --> (255 + 1) / 125000 = 2.048 ms
      min time base is 0.016 ms if TIM4_PERIOD = 1   --> (  1 + 1) / 125000 = 0.016 ms
  - In this example we need to generate a time base equal to 1 ms
   so TIM4_PERIOD = (0.001 * 125000 - 1) = 124 */

  /* Time base configuration */
  TIM4_TimeBaseInit(TIM4_PRESCALER_128, 124);
  /* Clear TIM4 update flag */
  TIM4_ClearFlag(TIM4_FLAG_UPDATE);
  /* Enable update interrupt */
//  TIM4_ITConfig(TIM4_IT_UPDATE, ENABLE);
  
//  /* enable interrupts */
//  enableInterrupts();

  /* Enable TIM4 */
  TIM4_Cmd(ENABLE);
}

/////////////#####################################
void TIM2_Config(void)
{
    /* TimerTick = 1�s
       Warning: fcpu must be equal to 16MHz
       fck_cnt = fck_psc/presc = fcpu/16 = 1MHz --> 1 tick every 1�s
       ==> 1 �s / 1�s = 1 ticks
     */
    TIM2_TimeBaseInit( TIM2_PRESCALER_32, 60000);
    
    TIM2_ClearFlag(TIM2_FLAG_UPDATE);

    TIM2_UpdateRequestConfig(TIM2_UPDATESOURCE_GLOBAL);

    TIM2_ITConfig(TIM2_IT_UPDATE, DISABLE);
    
//    enableInterrupts();
    
    TIM2_Cmd(ENABLE);
}
/**
  * @brief  Decrements the TimingDelay variable.
  * @param  None
  * @retval None
  */
void TimingDelay_Decrement(void)
{
  if (TimingDelay != 0x00)
  {
    TimingDelay--;
  }
}
/**
  * @brief Delay
  * @param nCount
  * @retval None
  */
void Delay_Basic(uint32_t nCount)
{
  /* Decrement nCount value */
  while (nCount != 0)
  {
    nCount--;
  }
}
PUTCHAR_PROTOTYPE
{
    /* Write a character to the UART1 */
    UART1_SendData8(c);
    /* Loop until the end of transmission */
    while (UART1_GetFlagStatus(UART1_FLAG_TXE) == RESET);

    return (c);
}

/**
  * @brief Retargets the C library scanf function to the USART.
  * @param None
  * @retval char Character to Read
  */
GETCHAR_PROTOTYPE
{
#ifdef _COSMIC_
  char c = 0;
#else
  int c = 0;
#endif
  /* Loop until the Read data register flag is SET */
  while (UART1_GetFlagStatus(UART1_FLAG_RXNE) == RESET);
    c = UART1_ReceiveData8();
  return (c);
}
#ifdef USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *   where the assert_param error has occurred.
  * @param file: pointer to the source file name
  * @param line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif

/**
  * @}
  */


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
