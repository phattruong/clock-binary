#include "led_config_b5.h"

void delay(uint32_t i)
{
  while(i-- >0);
}

void Led_Init_GPIOB_PIN_5()
{
  GPIO_Init(LED_PORT, LED_PIN, GPIO_MODE_OUT_PP_LOW_FAST);
}

void Led_Blink_GPIOB_PIN_5(uint32_t i)
{
  GPIO_WriteReverse(LED_PORT, LED_PIN);
  while(i-- >0);
}

void Led_On_GPIOB_PIN_5()
{
  GPIO_WriteLow(LED_PORT, LED_PIN);
}

void Led_Off_GPIOB_PIN_5()
{
  GPIO_WriteHigh(LED_PORT, LED_PIN);
}